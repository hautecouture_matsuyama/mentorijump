﻿using System;
using UnityEngine;

public static class TrajectoryHelper
{

	private const float minDistance = 0.4f; // minimum distance between each point on trajectory

	public static Vector3 Iterate (Vector3 planet0, float radius0, float radius1, float angle0, float speed, float gravity, float deltaX, float space0, float space1, int iterations, Action <int, Vector3> callback)
	{
		var normal = Quaternion.Euler (0.0f, 0.0f, angle0) * Vector3.up;
		var initialVelocity = normal * speed;
		var initialPosition = planet0 + normal * radius0;
		var force = Vector3.up * gravity;

		var impactTime = deltaX / initialVelocity.x;

		//	Spacing from first planet
		var startIteratingTime = CalculateStartT (normal, space0, gravity, initialVelocity);

		//	Impact data
		var finalPosition = DerivePosition (initialPosition, initialVelocity, force, impactTime);
		var finalVelocity = DeriveVelocity (initialVelocity, force, impactTime );

		//	Spacing from last planet
		var stopIteratingTime = CalculateStopT (initialPosition, finalPosition, -finalVelocity.normalized, radius1 + space1, finalVelocity);

		//	Iterate curve
		for (int i = 0; i < iterations; ++i)
		{
			var t = (float)i / (float)(iterations - 1);
			var time = Mathf.Lerp (startIteratingTime, stopIteratingTime, t);
			var position = DerivePosition (initialPosition, initialVelocity, force, time);

			callback (i, position);
		}

		return finalPosition;
	}

	public static Vector3 Iterate (Vector3 planet0, float radius0, float radius1, float angle0, float speed, float gravity, float deltaX, float space0, float space1, int iterations, Trajectory _t)
	{
		var normal = Quaternion.Euler (0.0f, 0.0f, angle0) * Vector3.up;
		var initialVelocity = normal * speed;
		var initialPosition = planet0 + normal * radius0;
		var force = Vector3.up * gravity;
		
		var impactTime = deltaX / initialVelocity.x;
		
		//	Spacing from first planet
		var startIteratingTime = CalculateStartT (normal, space0, gravity, initialVelocity);
		
		//	Impact data
		var finalPosition = DerivePosition (initialPosition, initialVelocity, force, impactTime);
		var finalVelocity = DeriveVelocity (initialVelocity, force, impactTime );
		
		//	Spacing from last planet
		var stopIteratingTime = CalculateStopT (initialPosition, finalPosition, -finalVelocity.normalized, radius1 + space1, finalVelocity);

		//	Iterate curve
		for (int i = 0; i < iterations; ++i)
		{
			var t = (float)i / (float)(iterations - 1);
			var time = Mathf.Lerp (startIteratingTime, stopIteratingTime, t);
			var position = DerivePosition (initialPosition, initialVelocity, force, time);
			
			_t.AddPoint (i, position);
		}
		
		return finalPosition;
	}

	private static Vector3 DeriveVelocity (Vector3 v0, Vector3 acceleration, float time)
	{
		return v0 + acceleration * time;
	}
	
	private static Vector3 DerivePosition (Vector3 p0, Vector3 v0, Vector3 acceleration, float time)
	{
		return p0 + (v0 * time) + (0.5f * acceleration * time * time);
	}
	
	private static float CalculateStartT (Vector3 normal, float space0, float gravity, Vector3 velocity)
	{
		var spacing0X = normal.x * space0;
		var spacing0Y = normal.y * space0;
		var startTX = spacing0X / velocity.x;
		var startTY = (-velocity.y + Mathf.Sqrt (Mathf.Pow (velocity.y, 2.0f) - 4 * (spacing0Y * gravity * 0.5f))) / (2 * gravity * 0.5f);
		
		return Mathf.Max (startTX, startTY);
	}
	
	private static float CalculateStopT (Vector3 initialPosition, Vector3 planetOrigin, Vector3 normal, float space1, Vector3 velocity)
	{
		var impactPoint = planetOrigin + (normal * space1);
		var deltaX = impactPoint.x - initialPosition.x;
		var time = deltaX / velocity.x;
		
		return time;
	}

	// -- 
	/// Returns the angle required for the projectile to reach it's target
	/// when the initial velocity is already set (i.e. "15 m/s").
	/// paths they can take; a higher angle (greater max height, longer duration. i.e. a mortar shell) 
	/// and a lower angle (lower max height, shorter duration. i.e. a bullet)</param>
	public static float GetAngle (float initialVelocity, Vector3 initialPosition, Vector3 targetPosition, bool useHighAngle)
	{
		Vector3 direction = targetPosition - initialPosition;
		Vector3 distance = direction;
		distance.y = 0;
		float x = distance.magnitude; //direction.z;
		float y = direction.y;
		float v = initialVelocity;
		float g = Physics.gravity.magnitude;
		
		float v2 = v * v;
		float v4 = v2 * v2;
		float x2 = x * x;
		
		if (useHighAngle)
		{
			float theta = Mathf.Atan2 (v2 + Mathf.Sqrt (v4 - g * (g * x2 + 2 * y * v2)), g * x);
			return theta * Mathf.Rad2Deg;
		}
		else
		{
			float theta = Mathf.Atan2 (v2 - Mathf.Sqrt (v4 - g * (g * x2 + 2 * y * v2)), g * x);
			return theta * Mathf.Rad2Deg;
		}
	}
}
