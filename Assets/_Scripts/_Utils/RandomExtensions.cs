﻿using UnityEngine;
using System.Collections;

public static class RandomExtensions
{
	//If you supply a value of 1 as the p parameter, you get uniform distribution. 
	//If you use 2, you get the quadratic behaviour. (decreses towards second value)
	//0.5 gives you square root (ie, higher numbers are favoured). 
	//You can use any fractional value for this, so you can vary the behaviour smoothly.
	public static float SkewedRandomRange (float start, float end, float p) 
	{
		//return (Mathf.Pow (Random.value, p)) * (start + end - start);
		return  Mathf.Pow (Random.value, p) * (end - start) + start;
	}

	public static int SkewedRandomRange (int start, int end, float p) 
	{
		return (int)Mathf.Pow (Random.value, p) * (end - start) + start;
	}
}

