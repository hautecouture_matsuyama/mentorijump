﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankManager : MonoBehaviour
{
    PlayFabManager PF;
    public GameObject PF_obj;

    public Text name_txt_start;
    public GameObject nameSet;

    public Text name_txt_change;
   // public GameObject nameSet_change;

   

    public GameObject rankSet;
    public int bestScore_server;
    public Text currentRank_txt;

    public Button name_set_btn;
    public Button name_set_btn_change;
    public GameObject name_set_panel;

    // Start is called before the first frame update
    void Start()
    {
        PF = PF_obj.GetComponent<PlayFabManager>();
        //GameManager.instance.userid_server  = "";
        //GameManager.instance.name_server = "";
        //GameManager.instance.login_int = 0;
        //PlayerPrefs.SetInt("_BestMeters", 0);
        //SaveInfo.SaveAllInfo();
        LoadInfo.LoadAllInfo();

        if (GameManager.instance.login_int == 0)
        {
            GameManager.instance.nameSet_int = 1;
            nameSet.SetActive(true);
        }
        else
        {

            Invoke("InvokeLoginNormal", 0.1f);

            //PF.LoginBtn();
            //nameSet.SetActive(false);
        }
    }

    void InvokeLoginNormal()
    {
        PF.LoginBtn();
        nameSet.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {
        if(GameManager.instance.nameSet_int ==1)NameButtonOnOff(name_txt_start,name_set_btn,GameManager.instance.nameSet_int);
        else if(GameManager.instance.nameSet_int ==2) NameButtonOnOff(name_txt_change, name_set_btn_change, GameManager.instance.nameSet_int);
    }

    public void NameButtonOnOff(Text nameInputText,Button name_set_button,int num)
    {
        if(num ==1)
        {
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                if (nameInputText.text.Length <= 5 && 2 < nameInputText.text.Length) name_set_btn.interactable = true;
                else name_set_button.interactable = false;
            }
        }
        else if(num == 2)
        {
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                if (nameInputText.text.Length <= 5 && 2 < nameInputText.text.Length) name_set_btn_change.interactable = true;
                else name_set_button.interactable = false;
            }
        }
        
    }

    public void StartNameSet()
    {
        string name_sub = name_txt_start.text;
        GameManager.instance.name_server = name_sub;

        Debug.Log("nameset" + GameManager.instance.name_server);

        PF.LoginMain(name_sub);
        nameSet.SetActive(false);
        //anima.SetBool("showStart", true);
        GameManager.instance.login_int = 1;
        GameManager.instance.nameSet_int = 0;
        SaveInfo.SaveAllInfo();
        //AC.NendOn();
        //play.SetActive(true);

    }

    public void ChangeNameSet()
    {
        string name_sub = name_txt_change.text;
        GameManager.instance.name_server = name_sub;

        Debug.Log("nameset" + GameManager.instance.name_server);

        PF.LoginMain(name_sub);
        name_set_panel.SetActive(false);
        //anima.SetBool("showStart", true);
        GameManager.instance.login_int = 1;
        GameManager.instance.nameSet_int = 0;
        SaveInfo.SaveAllInfo();
        //AC.NendOn();
        //play.SetActive(true);

    }
    /*
    void InvokeNameSet()
    {

    }*/

    public void ShowRanking()
    {
        rankSet.SetActive(!rankSet.activeSelf);

    }

    public void ShowNameSet()
    {
        name_set_panel.SetActive(!name_set_panel.activeSelf);
    }

    public void SetRanking(int num)
    {
        PF.SetRank(num);
    }


    public void InvokeOver()
    {
        PF.GetStat();
        Invoke("InvokeSet", 2f);
    }

    public void InvokeSet()
    {
        RankSet();
    }

    public void RankSet()
    {
        currentRank_txt.text = PF.result_rank.ToString();
    }
}
