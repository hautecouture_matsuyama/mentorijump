﻿using UnityEngine;
using System.Collections;

public class Back_kikyu : MonoBehaviour {

    private float Xmove_Speed;
    private float Xmove;
    private float Ymove;
    public bool Turn;
    public float Marzin;


    // Update is called once per frame
    void Update()
    {
        Vector3 Scale = transform.localScale;

        Marzin += 1 * Time.deltaTime;

        if (Turn == false)
        {
            Xmove_Speed = -0.1f;
            Scale.x = 0.5f;
        }
        if (Turn == true)
        {
            Xmove_Speed = +0.1f;
            Scale.x = -0.5f;
        }

        if (Marzin > 150)
        {
            if (Turn == false)
            {
                Turn = true;
                Marzin = 0;
            }

            else if (Turn == true)
            {
                Turn = false;
                Marzin = 0;
            }
        }




        Xmove = Xmove_Speed;
        transform.localScale = Scale;
        transform.Translate(0, Xmove*Time.deltaTime, 0, Space.World);
    }
}
