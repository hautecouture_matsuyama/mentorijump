﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseButton : MonoBehaviour {

    [SerializeField]
    private GameObject _closeObejct;

    public void Close()
    {
        _closeObejct.SetActive(false);
        Game.instance.ResumeGame();
    }
}
