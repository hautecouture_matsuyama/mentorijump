﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


//120401
//using Firebase;
//using Firebase.Database;
//using Firebase.Unity.Editor;
//120401

public class RankingManager : SingletonMonoBehaviour<RankingManager>
{
    
    //今後新しいグループを追加したい場合は新規に記述
    private enum RANKING_GROUP
    {
        HC_GAMES,
        TUNER_JAPAN,
        OTHER,
    }

    //FirebaseのデータベースURL
    [SerializeField]
    private string _databaseUrl;

    [SerializeField]
    private RANKING_GROUP _rankingGroup;

    [SerializeField]
    private string _rankingChildItem;


    [SerializeField]
    private GameObject _content;
    [SerializeField]
    private GameObject _loadPanel;
    [SerializeField]
    private GameObject _createNameCanvas;
    [SerializeField]
    private GameObject _rankingCanvas;
    [SerializeField]
    private Button _editOKButton;
    [SerializeField]
    private Button _closeButton;
    [SerializeField]
    private Button _showRankingButton;

    [SerializeField]
    private Text _myrankText;

    [SerializeField]
    private InputField _inputfield;

    //120401
    /*
    private DatabaseReference _rankingDb;
    private DatabaseReference _idListDb;
    */
    //120401

    private List<GameObject> _rankList = new List<GameObject>();
    private List<GameObject> _nameList = new List<GameObject>();
    private List<GameObject> scoreList = new List<GameObject>();
    private List<GameObject> IDList = new List<GameObject>();
    
    //後で消す
    private System.Random random = new System.Random();

    private int day;
    private int CourseDay = 0;//月曜からの経過日
    private DateTime lastDate;
    private TimeSpan daysNum;
    private DateTime date;


    void Awake()
    {
        if (this != Instance)
        {
            Destroy(this);
            return;
        }

        Initialize();

        Debug.Log("ユーザーIDは" + PlayerPrefs.GetString("userid"));
    }

    //サインアップ用関数
    public void SignUp(string name)
    {
        Guid guid = Guid.NewGuid();
        string userid = guid.ToString();

        PrefsManager.SetUserId(userid.Substring(0, 8));
        PrefsManager.SetUserName(name);

        Dictionary<string, object> itemMap = new Dictionary<string, object>();
        
        itemMap.Add("name", name);
        itemMap.Add("score", 1);

        Dictionary<string, object> map = new Dictionary<string, object>();
        map.Add(userid.ToString() , itemMap);
        //データ更新！

        //120401
        //_rankingDb.UpdateChildrenAsync(map);
        //120401

        _createNameCanvas.gameObject.SetActive(false);
    }


    //アカウント判定
    //既にユーザーIDがあれば、"true"が返ってくる
    public bool SignIn()
    {
        return PlayerPrefs.HasKey("userid");
    }

    //初期化用関数
    private void Initialize()
    {
        //120401
        /*
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(_databaseUrl);
        _rankingDb = FirebaseDatabase.DefaultInstance.GetReference(_rankingGroup.ToString()).Child(_rankingChildItem);
        */
        //120401

        ServerTime server = new ServerTime();
        StartCoroutine(server.GetTime());

        //ランキングのノードをすべて取得
        foreach (var n in _content.gameObject.GetChildren())
        {
            //各リストに初期値挿入
            switch (n.name)
            {
                case "rText":
                    _rankList.Add(n);
                    break;
                case "nText":
                    _nameList.Add(n);
                    break;
                case "sText":
                    scoreList.Add(n);
                    break;
                case "uidText":
                    IDList.Add(n);
                    break;
            }

        }


        //201207
        /*
        //初期値代入
        for (int i = 0; i < _rankList.Count; i++)
        {
            _rankList[i].GetComponent<Text>().text = i + 1 + "位";
            _nameList[i].GetComponent<Text>().text = "Player";
            scoreList[i].GetComponent<Text>().text = "" + 0;
        }

        //既に登録しているか？
        if (!SignIn())
        {
            _createNameCanvas.SetActive(true);
            _inputfield.text = SetDefaultName();
        }
            
        
        _editOKButton.onClick.AddListener( () => SignUp(_inputfield.text));
        _showRankingButton.onClick.AddListener(ShowRanking);
        _closeButton.onClick.AddListener(CloseButton);
        */
        //201207

    }


    //スコア送信関数
    
    public void SendScore(int score)
    {
        string userid = PrefsManager.GetUserId();
        string name = PrefsManager.GetUserName();

            var id = userid.Substring(0, 8);
            PrefsManager.SetUserId(id);
            userid = PrefsManager.GetUserId();

            Dictionary<string, object> itemMap = new Dictionary<string, object>();

            itemMap.Add("name", name);
            itemMap.Add("score", score);

            Dictionary<string, object> map = new Dictionary<string, object>();
            map.Add(userid, itemMap);

            //120401
            //_rankingDb.UpdateChildrenAsync(map);
            //120401
    }
    
    //ランキングの取得
    public void GetRankingData()
    {
        _loadPanel.SetActive(true);

        List<string> _scoreList = new List<string>();
        List<string> nameList = new List<string>();
        List<string> _idList = new List<string>();


        //120401
        /*
        _rankingDb.OrderByChild("score").LimitToLast(100).GetValueAsync()
            .ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    //失敗した時
                    UnityEngine.Debug.Log("失敗した");
                }

                else if(task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    IEnumerator<DataSnapshot> en = snapshot.Children.GetEnumerator();

                    UnityEngine.Debug.Log("読み込むよ");

                    List<DataSnapshot> list = new List<DataSnapshot>();
                    list.Add(snapshot);

                    while (en.MoveNext())
                    {
                        DataSnapshot data = en.Current;
                        nameList.Add((string)data.Child("name").GetValue(true));
                        _scoreList.Add(data.Child("score").GetValue(true).ToString());
                        _idList.Add(data.Key);
                        UnityEngine.Debug.Log("読込中");
                    }

                    _loadPanel.SetActive(false);

                    //降順に並び替え
                    _scoreList.Reverse();
                    nameList.Reverse();
                    _idList.Reverse();

                    for (int i = 0; i < 100; i++)
                    {
                        scoreList[i].GetComponent<Text>().text = _scoreList[i];
                        _nameList[i].GetComponent<Text>().text = (int.Parse(_scoreList[i]) == 0) ? "Player" : _nameList[i].GetComponent<Text>().text = nameList[i];
                        IDList[i].GetComponent<Text>().text = (int.Parse(_scoreList[i]) == 0) ? null : _idList[i];

                        if (_idList[i] == PrefsManager.GetUserId() && (int.Parse(_scoreList[i]) != 0))
                        {
                            scoreList[i].transform.parent.GetComponent<Image>().color = Color.yellow;
                        }
                        else
                        {
                            scoreList[i].transform.parent.GetComponent<Image>().color = new Color(0.42f, 0.2f, 0.075f, 0);
                        }

                        _myrankText.text = GetMyRankingData();
                    }
                }

            });
            */
            //120401


    }

    public string GetMyRankingData()
    {
        string myScore = "";
        string myUserID = PrefsManager.GetUserId();
        int scoreCnt =0;

        while(scoreCnt < 100)
        {
            if(IDList[scoreCnt].GetComponent<Text>().text == myUserID)
            {
                myScore = (scoreCnt + 1).ToString();

				return myScore + "位";
            }

            scoreCnt++;
        }

        return "100位以下";
    }

    //デフォルトネームをセットする
    public string SetDefaultName()
    {
        string defaultName = "たろう";

        TextAsset nameTextFile = Resources.Load("Text/defaultname", typeof(TextAsset)) as TextAsset;
        string[] nameList = nameTextFile.text.Split("\n"[0]);
        int idx = random.Next(nameList.Length);
        defaultName = nameList[idx].Replace("\r", "");
        return defaultName;
    }

    //デバッグ用サインアップ
    public void DebugSignUp(string name)
    {
        Guid userid = Guid.NewGuid();
        PlayerPrefs.SetString("userid", userid.ToString());
        PlayerPrefs.SetString("username", name);

        Dictionary<string, object> itemMap = new Dictionary<string, object>();

        name = SetDefaultName();
        int score = random.Next(1000);

        itemMap.Add("name", name);
        itemMap.Add("score", score);

        Dictionary<string, object> map = new Dictionary<string, object>();
        map.Add(userid.ToString(), itemMap);
        //データ更新！

        //120401
        //_rankingDb.UpdateChildrenAsync(map);
        //120401
    }


    //ランキングの表示
    public void ShowRanking()
    {
        _rankingCanvas.SetActive(true);
        GetRankingData();
    }

    //名前登録関数
    private void CreateName()
    {
        _createNameCanvas.SetActive(true);
    }

    //名前変更
    private void ChangeName()
    {
        string userid = PrefsManager.GetUserId();

        Dictionary<string, object> itemMap = new Dictionary<string, object>();

        //itemMap.Add("name", _inputfield.text);
        //itemMap.Add("score", PrefsManager.GetWeeklyBestScore());

        Dictionary<string, object> map = new Dictionary<string, object>();
        map.Add(userid, itemMap);

        //120401
        //_rankingDb.UpdateChildrenAsync(map);
        //120401

    }

    private void CloseButton()
    {
        _rankingCanvas.SetActive(false);
    }

    private void GetMyrankingButton()
    {
        
    }

    public void DebugSignUp(int cnt, int wait)
    {
        StartCoroutine(SignUp(cnt, wait));

    }

    IEnumerator SignUp(int cnt, int wait)
    {
        int idx = 1;
        while (true)
        {
            int i = 0;

            while (i < cnt)
            {
                DebugSignUp("田中");

                i++;
            }
            _myrankText.text = idx + "回目";
            idx++;
            yield return new WaitForSeconds(wait);
        }
    }

    //private void DebugSendScore()
    //{
    //    SendScore(1000);
    //}

}

