﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using LitJson;

public class MessageController : MonoBehaviour {
	class ViewState {
		public static int IndexLauncher = 0;
		public static int IndexList = 1;
		public static int IndexDetail = 2;
		
		public string OpenMethod { get; set; }
		public string CloseMethod { get; set; }
		public bool Enabled { get; set; }
		
		public ViewState(string open, string close, bool enabled) {
			OpenMethod = open;
			CloseMethod = close;
			Enabled = enabled;
		}
	}

	[SerializeField]
	public RectTransform MessageListNodePrefab;

	public RectTransform MessageLauncherTransform;
	public RectTransform MessageListTransform;
	public RectTransform MessageDetailTransform;
	public RectTransform MessageListContentTransform;
	
	private List<ViewState> viewStates = new List<ViewState> {
		new ViewState("OpenLauncher", "CloseLauncher", true),
		new ViewState("OpenList", "CloseList", true),
		new ViewState("OpenDetail", "CloseDetail", true)
	};

	//private FelloPush fello;
	private int currentIndex = 0;
	private string currentMessageId = null;

	void Start () {
		//fello = GameObject.Find ("FelloPush").GetComponent<FelloPush>();
	}

	void Update () {
	}

	public void OpenView (int index) {
		if (index <= 0 || index > viewStates.Count) {
			throw new System.ArgumentException("view index out of range", "index");
		}
		// 既に最初のビューが表示されている場合
		if (currentIndex == 0 && index == 0) {
			return;
		}
		// 既に最後のビューが表示されている場合
		if (currentIndex == viewStates.Count && index == viewStates.Count) {
			return;
		}
		// 指定されたビューを開く
		for (int i=currentIndex; i<index; i++) {
			ViewState curr = viewStates[i];
			ViewState next = viewStates[i+1];
			Invoke (curr.CloseMethod, 0);
			Invoke (next.OpenMethod, 0);
		}
		// 現在開いているビューのインデックスを更新
		currentIndex = index;
	}

	public void OpenViewForDetail(string messageId) {
		// お知らせの内容を開く
		currentMessageId = messageId;
		OpenView (ViewState.IndexDetail);
	}

	public void CloseView () {
		if (currentIndex <= 0) {
			return;
		}
		ViewState curr = viewStates[currentIndex];
		ViewState prev = viewStates[currentIndex-1];
		Invoke (curr.CloseMethod, 0);
		Invoke (prev.OpenMethod, 0);
		currentIndex--;
	}

	private void OpenLauncher() {
		MessageLauncherTransform.gameObject.SetActive (true);
	}

	private void CloseLauncher() {
		MessageLauncherTransform.gameObject.SetActive (false);
	}

	private void OpenList() {
		MessageListTransform.gameObject.SetActive (true);
		ReloadMessages();
		//fello.UpdateMessages();
	}

	private void ReloadMessages() {
        /*
		foreach (Transform child in MessageListContentTransform) {
			GameObject.Destroy(child.gameObject);
		}

		IList<Message> messages = fello.GetStoredMessages();
		foreach (Message message in messages) {
			string messageId = message.message_id;
			// お知らせのタイトル
			string title = message.title;
			// お知らせ本文のURL、このコード内では使用しないためコメントアウト
			//string url = message.url;
			// お知らせ配信時に付与したデータ、このコード内では使用しないためコメントアウト
			//string extra = message.extra;
			// お知らせ配信日
			DateTime baseDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			string deliveredAt = baseDateTime.AddSeconds(message.delivered_at).ToLocalTime().ToString("yyyy/M/d H:mm:ss");
			// お知らせが既読かどうか
			bool read = message.read == 1;

			RectTransform item = GameObject.Instantiate(MessageListNodePrefab) as RectTransform;
			item.GetComponentInChildren<Button>().onClick.AddListener(() => OpenViewForDetail(messageId));
			item.Find ("Button/Title").gameObject.GetComponent<Text>().text = title;
			item.Find ("Button/Date").gameObject.GetComponent<Text>().text = deliveredAt;
			item.Find ("Button/Read").gameObject.SetActive(!read);
			item.SetParent(MessageListContentTransform, false);
		}
		// 未読お知らせ数を表示
		int unreadCount = fello.GetUnreadMessageCount();
		SetUnreadCount(unreadCount);
        */
	}

	public void SetUnreadCount(int unreadCount) {
		string baseText = "お知らせ一覧を開く";
		GameObject target = MessageLauncherTransform.Find ("Container/Button/Text").gameObject;
		if (unreadCount == 0) {
			target.GetComponent<Text>().text = baseText;
		} else {
			target.GetComponent<Text>().text = baseText + " (未読 " + unreadCount + " 件)";
		}
	}

	private void CloseList() {
		MessageListTransform.gameObject.SetActive (false);
		foreach (Transform child in MessageListContentTransform) {
			GameObject.Destroy(child.gameObject);
		}
	}

	private void OpenDetail() {
		if (currentMessageId == null || currentMessageId.Equals("")) {
			return;
		}
        /*
		MessageDetailTransform.gameObject.SetActive (true);
        Message message = fello.GetMessage(currentMessageId);
        WebViewObject webView = MessageDetailTransform.gameObject.GetComponentInChildren<WebViewObject>();
		webView.Init ((msg) => {
			Debug.Log(msg);
		});
		webView.SetMargins(10, 100, 10, 10);
        webView.LoadURL(message.url);
        webView.SetVisibility (true);
		fello.MarkMessagesRead(new List<string> {currentMessageId});
        fello.UpdateMessages();
        */
	}
    /*
	private void CloseDetail() {
		MessageDetailTransform.gameObject.GetComponentInChildren<WebViewObject>().SetVisibility (false);
		MessageDetailTransform.gameObject.SetActive (false);
	}*/

	public void onLaunchFromNotification(string param) {
		Debug.Log("MessageController onLaunchFromNotification:" + param);
	}
    /*
	public void onLaunchFromMessage(string param) {
		Debug.Log("MessageController onLaunchFromMessage:" + param);
		CallbackParams data = JsonMapper.ToObject<CallbackParams>(param);
		string id = data.id;
		fello.MarkMessagesRead(new List<string> {id});
		Message message = fello.GetMessage(id);
		string messageId = message.message_id;
		OpenViewForDetail(messageId);
	}
    */
	public void onUpdateMessages() {
		Debug.Log("MessageController onUpdateMessages");
		ReloadMessages();
	}
}
