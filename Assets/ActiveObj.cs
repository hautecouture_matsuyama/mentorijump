﻿using UnityEngine;
using System.Collections;

public class ActiveObj : MonoBehaviour {

    [SerializeField]
    private GameObject aciveObject;

    public void Active()
    {
        GameManager.instance.nameSet_int = 2;
        aciveObject.SetActive(true);
    }
}
